//
//  AppDelegate.swift
//  YogaLayout
//
//  Created by Dhruvi on 19/07/20.
//  Copyright © 2020 Arth Gajjar. All rights reserved.
//

import UIKit
import JavaScriptCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let context = JSContext()

        guard let commonJSPath = Bundle.main.path(forResource: "index", ofType: "js") else {
            print("Unable to read resource files.")
            return false
        }

        do {
            let common = try String(contentsOfFile: commonJSPath, encoding: String.Encoding.utf8)
            // running index.js
            context?.evaluateScript(common)
        } catch (let error) {
            print("Error while processing index.js : \(error)")
        }
        
        // Calling JS from Swift
        let console = context?.objectForKeyedSubscript("printingOnConsole");
        console?.call(withArguments: ["Swift wants to share", "Greetings from Apple"])
        
        
        
        let printUsingSwift: @convention(block) () -> Void = { print("in Swift") }
        // Calling Swift from JS
        context?.setObject(JSValue(object: unsafeBitCast(printUsingSwift, to: AnyObject.self), in: context), forKeyedSubscript: "printUsingSwift" as NSCopying & NSObjectProtocol)
        context?.evaluateScript("printUsingSwift")?.call(withArguments: [])
        
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = ViewController()
        window.backgroundColor = .white
        window.makeKeyAndVisible()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

