//
//  ViewController.swift
//  YogaLayout
//
//  Created by Dhruvi on 19/07/20.
//  Copyright © 2020 Arth Gajjar. All rights reserved.
//

import UIKit
import YogaKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.configureLayout { (layout) in
          layout.isEnabled = true
          layout.width = YGValue(self.view.bounds.size.width)
          layout.height = YGValue(self.view.bounds.size.height)
          layout.alignItems = .center
        }
        
        let contentView = UIView()
        contentView.configureLayout { (layout) in
          layout.isEnabled = true
          layout.flexDirection = .row
          layout.width = YGValue(self.view.bounds.size.width)
          layout.height = YGValue(self.view.bounds.size.height)
        }
        
        let child1 = UIView()
        child1.backgroundColor = .red
        child1.configureLayout{ (layout)  in
          layout.isEnabled = true
          layout.width = 80
        }
        contentView.addSubview(child1)
        
        let child2 = UIView()
        child2.backgroundColor = .blue
        child2.configureLayout{ (layout)  in
          layout.isEnabled = true
          layout.width = 80
          layout.flexGrow = 1
        }
        contentView.addSubview(child2)
        
        view.addSubview(contentView)
        
        view.yoga.applyLayout(preservingOrigin: true)
    }


}
